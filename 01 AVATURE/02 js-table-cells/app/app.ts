((d) => {
	const tables = d.querySelectorAll('table');
	console.log(tables);
	var cells: number[] = [];
	var ids: any[] = [];

	tables.forEach((table) => {
		let numCells = table.querySelectorAll('td').length;
		cells.push(numCells); // en este array se guardan todos los valores de número de celdas de las distintas tablas
		ids[numCells] = table.id; // podríamos haber hecho un objeto de tipo:
		// arrayNumcells-id ={[numCells: 9, id: "idX"], [numCells: 6, id: "idY"], [numCells: 9, id: "idz"]}
	});

	console.log(cells.sort((a, b) => b - a)); // ordenado de mayor a  menor
	let numCellsBigger = cells[0];
	let idBiggerTable = ids[numCellsBigger];
	console.log(`La tabla cuyo id es: ${idBiggerTable}, es la que más celdas tiene, un total de: ${numCellsBigger}`);
	// WARNING: falataría comprobar las tablas más grandes con el mismo número de celdas!!!
})(document);
