"use strict";
(function (d) {
    var tables = d.querySelectorAll('table');
    console.log(tables);
    var cells = [];
    var ids = [];
    tables.forEach(function (table) {
        var numCells = table.querySelectorAll('td').length;
        cells.push(numCells); // en este array se guardan todos los valores de número de celdas de las distintas tablas
        ids[numCells] = table.id; // podríamos haber hecho un objeto de tipo:
        // arrayNumcells-id ={[numCells: 9, id: "idX"], [numCells: 6, id: "idY"], [numCells: 9, id: "idz"]}
    });
    console.log(cells.sort(function (a, b) { return b - a; })); // ordenado de mayor a  menor
    var numCellsBigger = cells[0];
    var idBiggerTable = ids[numCellsBigger];
    console.log("La tabla cuyo id es: " + idBiggerTable + ", es la que m\u00E1s celdas tiene, un total de: " + numCellsBigger);
    // WARNING: falataría comprobar las tablas más grandes con el mismo número de celdas!!!
})(document);
